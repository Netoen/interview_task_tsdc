# This is a python script to bring up a cluster of Cassandra + Spark workers

## Requirements
python3.10+, jinja2. You can install jinja2 like this:
```bash
pip install -r reqs.pip
```
While git is not required (you can just download an archive copy) - you can easily download this repository using this command if you have it:
```
git clone https://gitlab.com/Netoen/interview_task_tsdc.git
```

## Usage
```bash
# Node and CPU values are positive integers,
# Cassandra RAM is a positive float value of GB (Java limitation),
# Spark RAM is a positive float value of GB,
# RAM and CPU are per replica values
python3 main.py \
--cassandra-nodes X \
--cassandra-cpu Y \
--cassandra-ram Z \
--spark-cpu A \
--spark-ram B
# or
python3 main.py -cn X -cc Y -cr Z -sc A -sr B
```
The resulting docker-compose.yml will appear in the ```docker``` directory. If you wish to adjust the way spark is built - its ```Dockerfile``` is in the same directory. If you wish adjust ```docker-compose.yml``` template it is in ```templates``` directory.

## Script also respects environmental variables, lower priority than command line arguments:
```bash
CASSANDRA_NODES
CASSANDRA_CPU
CASSANDRA_RAM
SPARK_CPU
SPARK_RAM
```

## There are default values for the aforementioned parameters if the respective argument wasn't supplied:
```
X (node replicas) = 1
Y, A (cassandra / spark cpu count) = 1
Z (cassandra ram) = 1
B (spark ram) = 0.25
```