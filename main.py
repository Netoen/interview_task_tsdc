#!/usr/bin/python3.10
"""
Author: Makhno Artem
"""
import logging
import argparse
from jinja2 import Environment, FileSystemLoader
from os import environ, path, makedirs
from collections import OrderedDict


class Node_Conf:
  cassandra_nodes: int
  cassandra_cpu: int
  cassandra_ram: int
  spark_ram: int
  spark_ram: float

  def __init__(self, cassandra_nodes, cassandra_cpu, cassandra_ram, spark_cpu, spark_ram):
    self.cassandra_nodes = cassandra_nodes
    self.cassandra_cpu = cassandra_cpu
    self.cassandra_ram = cassandra_ram
    self.spark_cpu = spark_cpu
    self.spark_ram = spark_ram


def main():
  # cassandra_nodes, cassandra_cpu, cassandra_ram, spark_cpu, spark_ram, loglevel = parse_arguments()
  *tmp_var, loglevel = parse_arguments()
  nc = Node_Conf(*tmp_var)
  del tmp_var
  log_ = setup_logger(loglevel if loglevel else logging.ERROR)
  check_initialization(log_, nc.cassandra_nodes, nc.cassandra_cpu, nc.cassandra_ram, nc.spark_cpu, nc.spark_ram)
  log_.info('Initialization successful')
  j2_template(nc)
  log_.info('Jinja2 templating successful')
  log_.info('Script execution successful')


def setup_logger(loglevel: str = logging.ERROR):
  logger = logging.getLogger('main.py')
  logger.setLevel(loglevel)
  fh = logging.FileHandler('main.log')
  fh.setLevel(loglevel)
  ch = logging.StreamHandler()
  ch.setLevel(loglevel)
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  fh.setFormatter(formatter)
  ch.setFormatter(formatter)
  logger.addHandler(fh)
  logger.addHandler(ch)
  return logger


def parse_arguments():
  env_ = {'CASSANDRA_NODES': '', 'CASSANDRA_CPU': '', 'CASSANDRA_RAM': '',
          'SPARK_CPU': '', 'SPARK_RAM': ''}
  defaults = (1, 1, 1, 1, 0.25)
  for key in env_.keys():
    if _ := environ.get(key):
      env_[key] = float(_) if key == 'SPARK_RAM' else int(_)
  # TODO: make sure received values are appropriate A < X < B
  parser = argparse.ArgumentParser(description='Creating a cluster of Cassandra and Spark workers')
  parser.add_argument('-cn', '--cassandra-nodes', metavar='X', type=int, dest='cassandra_nodes',
                      help='Number of Cassandra nodes')
  parser.add_argument('-cc', '--cassandra-cpu', metavar='Y', type=int, dest='cassandra_cpu',
                      help='Number of Cassandra CPU cores')
  parser.add_argument('-cr', '--cassandra-ram', metavar='Z', type=int, dest='cassandra_ram',
                      help='Number of Cassandra allocated RAM')
  parser.add_argument('-sc', '--spark-cpu', metavar='A', type=int, dest='spark_cpu',
                      help='Number of Spark CPU cores')
  parser.add_argument('-sr', '--spark-ram', metavar='B', type=float, dest='spark_ram',
                      help='Number of Spark allocated RAM')
  parser.add_argument('--log', type=str, dest='loglevel',
                      help='Logging level for the application')
  args = vars(parser.parse_args())
  for env_var, arg_var, default in zip(env_.keys(), args.keys(), defaults):
    if not args[arg_var]:
      if not env_[env_var]:
        args[arg_var] = default
      else:
        args[arg_var] = env_[env_var]
  return args['cassandra_nodes'], args['cassandra_cpu'], args['cassandra_ram'], \
         args['spark_cpu'], args['spark_ram'], args['loglevel']


def check_initialization(logger, cassandra_nodes, cassandra_cpu, cassandra_ram, spark_cpu, spark_ram):
  msg_initialization_failed = 'Initialization failed, terminating'
  try:
    logger.debug(f'Arguments: '
                 f'cassandra_nodes = {cassandra_nodes}, '
                 f'cassandra_cpu = {cassandra_cpu}, '
                 f'cassandra_ram = {cassandra_ram}, '
                 f'spark_cpu = {spark_cpu}, '
                 f'spark_ram = {spark_ram}')
    assert cassandra_nodes and cassandra_cpu and cassandra_ram and spark_cpu and spark_ram
  except (NameError, AssertionError) as e:
    logger.critical('Application lacks a critical initialization variable, '
                    'run app with --log=DEBUG and look through log file')
    logger.critical(msg_initialization_failed)
    raise RuntimeError(msg_initialization_failed) from e


def j2_template(nc: Node_Conf):
  docker_compose_file = 'docker-compose.yml'
  docker_compose_template_file = docker_compose_file + '.j2'
  if not (path.exists('templates') or path.exists('templates/' + docker_compose_template_file)):
    raise RuntimeError(f'the templates directory or template {docker_compose_file} inside does not exist, terminating')
  file_loader = FileSystemLoader('templates')
  env = Environment(loader=file_loader)
  template = env.get_template(docker_compose_template_file)
  artifact = template.render(nc=nc)
  if not path.exists('docker'):
    makedirs('docker')
  with open('docker/' + docker_compose_file, 'w') as f:
    f.write(artifact)


if __name__ == '__main__':
  main()
